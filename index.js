require("dotenv").config();
let express = require("express");
let bodyParser = require("body-parser");
let path = require("path");
let cors = require("cors");
let http = require("http");
let hbs = require("express-handlebars");

// Init App
var app = express();
var server = http.createServer(app);

//Use CORS
app.use(cors());

// Use body-parser to get POST requests for API use
app.use(
    bodyParser.json({
        limit: "50mb"
    })
);
app.use("/assets", express.static(path.join(__dirname, "assets")));
// Handle 500
app.use(function (error, req, res, next) {
    if (500 === err.status) {
        res.render("500", {
            layout: null
        });
    }
});
// Routes setup
let publicRoutes = require("./routes/index");

app.use("/", publicRoutes);

// View Engine
app.set("views", path.join(__dirname, "views"));
app.engine(
  "hbs",
  hbs({
    extname: "hbs",
    defaultLayout: "main",
    layoutsDir: __dirname + "/views/layouts/",
    partialsDir: __dirname + "/views/partials/"
  })
);
app.set("view engine", "hbs");

app.set("port", process.env.PORT || 80);

server.listen(app.get("port"), function() {
  console.log("Server started on port " + app.get("port"));
});
