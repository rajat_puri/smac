let express = require("express");
let router = express.Router();
let path = require("path");
const Handlebars = require("handlebars");

Handlebars.registerHelper("prod", () => {
  // //console.log("came in here....header");
  return process.env.NODE_ENV === "prod";
});

Handlebars.registerHelper("apiBaseURL", () => {
  if (process.env.NODE_ENV === "prod")
    return process.env.PROD_API_MASTERUNION_ORG;
  return process.env.DEV_API_MASTERUNION_ORG;
});
router.get("/", function (req, res, next) {

  res.render("index", {
    title: "Index page",
    isProd: process.env.NODE_ENV === "prod",
    description: "smac index",
    metaData: {
      img: "https://cdn.mastersunion.org/assets/img/logo.png",
      url: "https://mastersunion.org",
    },
  });
});

router.get("/faq", function (req, res, next) {

  res.render("faq", {
    title: "Faq",
    isProd: process.env.NODE_ENV === "prod",
    description: "smac index",
    metaData: {
      img: "https://cdn.mastersunion.org/assets/img/logo.png",
      url: "https://mastersunion.org",
    },
  });
});

router.get("/career-services", function (req, res, next) {

  res.render("career-services", {
    title: "career-services",
    isProd: process.env.NODE_ENV === "prod",
    description: "smac index",
    metaData: {
      img: "https://cdn.mastersunion.org/assets/img/logo.png",
      url: "https://mastersunion.org",
    },
  });
});

router.get("/about-us", function (req, res, next) {

  res.render("about-us", {
    title: "about-us",
    isProd: process.env.NODE_ENV === "prod",
    description: "smac index",
    metaData: {
      img: "https://cdn.mastersunion.org/assets/img/logo.png",
      url: "https://mastersunion.org",
    },
  });
});

router.get("/course", function (req, res, next) {

  res.render("course", {
    title: "about-us",
    isProd: process.env.NODE_ENV === "prod",
    description: "smac index",
    metaData: {
      img: "https://cdn.mastersunion.org/assets/img/logo.png",
      url: "https://mastersunion.org",
    },
  });
});


router.get("/student-life", function (req, res, next) {

  res.render("student-life", {
    title: "student-life",
    isProd: process.env.NODE_ENV === "prod",
    description: "smac index",
    metaData: {
      img: "https://cdn.mastersunion.org/assets/img/logo.png",
      url: "https://mastersunion.org",
    },
  });
});

router.get("/roadmap", function (req, res, next) {

  res.render("roadmap", {
    title: "roadmap",
    isProd: process.env.NODE_ENV === "prod",
    description: "smac index",
    metaData: {
      img: "https://cdn.mastersunion.org/assets/img/logo.png",
      url: "https://mastersunion.org",
    },
  });
});

router.get("/for-companies", function (req, res, next) {

  res.render("for-companies", {
    title: "for-companies",
    isProd: process.env.NODE_ENV === "prod",
    description: "smac index",
    metaData: {
      img: "https://cdn.mastersunion.org/assets/img/logo.png",
      url: "https://mastersunion.org",
    },
  });
});

router.get("/research", function (req, res, next) {

  res.render("research", {
    title: "research",
    isProd: process.env.NODE_ENV === "prod",
    description: "smac index",
    metaData: {
      img: "https://cdn.mastersunion.org/assets/img/logo.png",
      url: "https://mastersunion.org",
    },
  });
});

router.get("/hostel", function (req, res, next) {

  res.render("hostel", {
    title: "hostel",
    isProd: process.env.NODE_ENV === "prod",
    description: "smac index",
    metaData: {
      img: "https://cdn.mastersunion.org/assets/img/logo.png",
      url: "https://mastersunion.org",
    },
  });
});

router.get("/contact-us", function (req, res, next) {

  res.render("contact-us", {
    title: "contact-us",
    isProd: process.env.NODE_ENV === "prod",
    description: "smac index",
    metaData: {
      img: "https://cdn.mastersunion.org/assets/img/logo.png",
      url: "https://mastersunion.org",
    },
  });
});


router.get("/events", function (req, res, next) {

  res.render("events", {
    title: "events",
    isProd: process.env.NODE_ENV === "prod",
    description: "smac index",
    metaData: {
      img: "https://cdn.mastersunion.org/assets/img/logo.png",
      url: "https://mastersunion.org",
    },
  });
});

router.get("/jobs", function (req, res, next) {

  res.render("jobs", {
    title: "jobs",
    isProd: process.env.NODE_ENV === "prod",
    description: "smac index",
    metaData: {
      img: "https://cdn.mastersunion.org/assets/img/logo.png",
      url: "https://mastersunion.org",
    },
  });
});


router.get("/cohort-statistics", function (req, res, next) {

  res.render("cohort-statistics", {
    title: "cohort-statistics",
    isProd: process.env.NODE_ENV === "prod",
    description: "smac index",
    metaData: {
      img: "https://cdn.mastersunion.org/assets/img/logo.png",
      url: "https://mastersunion.org",
    },
  });
});


router.get("/masters", function (req, res, next) {

  res.render("masters", {
    title: "masters",
    isProd: process.env.NODE_ENV === "prod",
    description: "smac index",
    metaData: {
      img: "https://cdn.mastersunion.org/assets/img/logo.png",
      url: "https://mastersunion.org",
    },
  });
});









module.exports = router;
